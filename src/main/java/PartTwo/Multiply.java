/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PartTwo;

/**
 *
 * @author Kunj
 */
public class Multiply extends Calculator {
    
    public Multiply( double o1, double o2){
        super(o1,o2);
    }
    
     public double multiply() {
        return operand1*operand2;
    }
}
