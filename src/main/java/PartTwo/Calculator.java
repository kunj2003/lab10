/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PartTwo;

/**
 *
 * @author Kunj
 */
public abstract class Calculator {
    protected double operand1;
    protected double operand2;
    
    public Calculator(double o1,double o2) {
        this.operand1=o1;
        this.operand2=o2;
    }
    
}
