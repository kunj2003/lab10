/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package PartOne;
import java.util.Date;

/**
 *
 * @author Kunj
 */
public class Employee 
{
    private String employeeId;
    private Date dateOfJoining;
    private double Salary;
    
    //no arg constructor
    public Employee(){}

    //arg constructor
    public Employee(String employeeId, Date dateOfJoining, double Salary) {
        this.employeeId = employeeId;
        this.dateOfJoining = dateOfJoining;
        this.Salary = Salary;
    }

    //getter(s)
    public Date getEmployeeDateOfJoining()
    {
        return dateOfJoining;
    }
      public String getEmployeeId()
    {
        return employeeId;
    }
      public double getEmployeeSalary()
      {
          return Salary;
      }
 }
