/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PartOne;

import java.util.Calendar;

/**
 *
 * @author Kunj
 */
public class EmployeeSingleDemo 
{
    public static void main(String[]args)
    {
        Calendar dateJoin = Calendar.getInstance();
        dateJoin.set( 2021, 0 , 22);
        
        Employee employee = new Employee("0001", dateJoin.getTime() , 40000);
        
        EmployeeTool tool = new EmployeeTool();
        
        System.out.println("IS PROMOTION DUE " +tool.isPromotionDueThisYear(employee, true));
        System.out.println("TAXES FOR THIS YEAR " +tool.calcIncomeTaxForCurrentYear(employee, 0.28));
       
    }
    
}