/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PartThree;

/**
 *
 * @author 16476
 */
public class Rectangle 
{
    private int width;
    private int height;

    //arg consrtuctor
    public Rectangle(int w, int h) {
        this.width = w;
        this.height = h;
    }

    //getter
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    //setter
     public void setWidth(int width) {
        this.width = width;
    }
    public void setHeight(int height) {
        this.height = height;
    }
    
    public int getArea()
    {
        return this.height * this.width;
    }
    
    public final static void setDimensions(Rectangle r, int w, int h)
    {
        
        r.setHeight(h);
        r.setWidth(h);
    }
}