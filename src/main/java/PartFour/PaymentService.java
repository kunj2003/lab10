/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PartFour;

/**
 *
 * @author Kunj
 */
public abstract class PaymentService
{
    private  double amount;
   

    //constructor
    public PaymentService(double amount) {
        this.amount = amount;
    }

    //getter
    public double getAmount() {
        return amount;
    }
    
    public abstract void processPayment(double amount);
    
    
}
