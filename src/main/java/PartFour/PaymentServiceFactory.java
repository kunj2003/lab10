/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PartFour;

/**
 *
 * @author Kunj
 */
public class PaymentServiceFactory {
   
    private static PaymentServiceFactory factory;
    
    //private default constructor --> so that  no one can instantiate from outside
    private PaymentServiceFactory()
    {}
    
    public static PaymentServiceFactory getInstance()
    {
        if(factory==null)
            factory = new PaymentServiceFactory();
        return factory;
    }
   
    public PaymentService getPaymentService(PaymentServiceType type)
    {
        switch( type )
        {
            case DEBIT : return new DebitPaymentService(200);
            case CREDIT : return new CreditPaymentService(400);
        }
        
        return null;
    }
}


