/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PartFour;

/**
 *
 * @author Kunj
 */
public class ShoppingCartDemo 
{
    public static void main(String []args)
    {
      PaymentServiceFactory factory=PaymentServiceFactory.getInstance();
      
      PaymentService debit=factory.getPaymentService(PaymentServiceType.DEBIT);
      PaymentService credit=factory.getPaymentService(PaymentServiceType.CREDIT);
      
      Cart cart=new Cart();
      cart.addProduct(new Product("Shirts ", 100));
      cart.addProduct(new product("Pants" , 100));
      
      cart.setPaymentService( creditServices );
      cart.payCart();
      
      cart.setPaymentService( debitServices );
      cart.payCart();
      
    }
    
}

