/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PartFour;

/**
 *
 * @author Kunj
 */
public class DebitPaymentService extends PaymentService
{

    public DebitPaymentService(double amount) {
        super(amount);
    }

    
    @Override
    public void processPayment(double amount) 
    {
        double output = amount;
        System.out.println("Processing Debit payment of " +output);
        
    }
    
    
}

